package com.everis.apirest.model.service;


import java.math.BigInteger;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;
import com.everis.apirest.model.repository.ProductoRepository;
import com.everis.apirest.model.repository.TipoProductoRepository;

@Transactional(readOnly = true)
@Service
public class ProductoServiceIpml implements ProductoService{

	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private TipoProductoRepository tipoproductoRepository;

	@Override
	public Iterable<Producto> listarProductos() {
		
		return productoRepository.findAll();
	}


	@Transactional(readOnly = false)
	@Override
	public Producto insertarProducto(Producto producto) throws Exception{
		
		TipoProducto tipoproducto = tipoproductoRepository.findByCodigo(producto.getTipoProducto().getCodigo()).orElseThrow(()-> new Exception("Tipo Producto no encontrado"));
		producto.setTipoProducto(tipoproducto);
		return productoRepository.save(producto);
	}




	@Override
	public Producto modificarProducto(Producto producto) throws Exception {
		
		
		return productoRepository.findByCodigo(producto.getCodigo())
		        .map(productoUpdate -> {
		        	productoUpdate.setNombre(producto.getCodigo());
		        	productoRepository.save(productoUpdate);
		            return productoUpdate;
		    })
		    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado"));
	}


	@Override
	public Producto obtenerProductoId(Long Id) throws Exception {
		
		return productoRepository.findById(Id).orElseThrow(()-> new Exception("Producto no encontrado"));
	}



	
}
