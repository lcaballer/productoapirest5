package com.everis.apirest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;
import com.everis.apirest.model.repository.ProductoRepository;


public interface ProductoService {
	

	public Iterable<Producto> listarProductos();
	
	public Producto obtenerProductoId(Long Id) throws Exception;
	
	public Producto modificarProducto(Producto producto) throws Exception;
	
	public Producto insertarProducto(Producto producto) throws Exception;
	

}
