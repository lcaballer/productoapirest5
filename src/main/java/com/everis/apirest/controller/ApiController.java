package com.everis.apirest.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.ProductoIGVResource;
import com.everis.apirest.controller.resource.ProductoReducidoResource;
import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.controller.resource.TipoProductoResource;
import com.everis.apirest.model.entity.Producto;
import com.everis.apirest.model.entity.TipoProducto;
import com.everis.apirest.model.service.ProductoService;


@RestController
public class ApiController {

	@Autowired
	ProductoService productoService;

	@Value("${igv}")
	BigDecimal igv;
	
	
	@GetMapping("/productos")
	
	public List<ProductoResource> listaProductos(){
		
		
		List<ProductoResource> listado = new  ArrayList();
		productoService.listarProductos().forEach(producto -> {
			
			ProductoResource productoResource = new ProductoResource();
			TipoProductoResource tipoProductoResource = new TipoProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setCodigo(producto.getCodigo());
			productoResource.setNombre(producto.getNombre());
			productoResource.setDescripcion(producto.getDescripcion());
			productoResource.setPrecio(producto.getPrecio());
			tipoProductoResource.setCodigo(producto.getTipoProducto().getCodigo());
			tipoProductoResource.setNombre(producto.getTipoProducto().getNombre());
			productoResource.setTipoProducto(tipoProductoResource);
			productoResource.setActivo(producto.getActivo());
			listado.add(productoResource);
		});
		
		return listado;
	}
	
	@GetMapping("/productos/{id}")
	public ProductoIGVResource obtenerProductoId(@PathVariable("id") Long id) throws Exception {
		
		BigDecimal calculo,pr;
		Producto producto = productoService.obtenerProductoId(id);
		
		ProductoIGVResource pigv = new ProductoIGVResource();
		
		pigv.setId(producto.getId());
		pigv.setCodigo(producto.getCodigo());
		pigv.setNombre(producto.getNombre());
		pigv.setPrecio(producto.getPrecio());
		pr = producto.getPrecio();
		calculo = pr.add(igv.multiply(pr));
		pigv.setPrecioNeto(calculo);
		
		return pigv;
	}
	
	
	@PutMapping("/productos/{codigo}")
	public ResponseEntity<ProductoResource> updateProducto(@PathVariable("codigo") String codigo, @Valid @RequestBody ProductoResource request) throws Exception {
		
		 Producto pr = new Producto();
		 pr.setCodigo(codigo);
		 pr.setNombre(request.getNombre());
		  
		 Producto pro = productoService.modificarProducto(pr);
		
		 ProductoResource productoResource = new ProductoResource();
		 productoResource.setId(pro.getId());
		 
		 
		 return new ResponseEntity<>(productoResource, HttpStatus.OK);
	}
	
	
	@PostMapping("/producto")
	public ProductoResource guardarCliente( @RequestBody ProductoReducidoResource productopost) throws Exception{
			
		
		Producto pr = new Producto();
		TipoProducto tp = new TipoProducto();
		pr.setCodigo(productopost.getCodigo());
		pr.setNombre(productopost.getNombre());
		pr.setDescripcion(productopost.getDescripcion());
		pr.setPrecio(productopost.getPrecio());
		tp.setCodigo(productopost.getCodigoTipoProducto());
		
		pr.setTipoProducto(tp);
		
		Producto producto = productoService.insertarProducto(pr);
		
		ProductoResource productoResource = new ProductoResource();
		TipoProductoResource tipoProductoResource = new TipoProductoResource();
		productoResource.setId(producto.getId());
		productoResource.setCodigo(producto.getCodigo());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setPrecio(producto.getPrecio());
		tipoProductoResource.setCodigo(producto.getTipoProducto().getCodigo());
		tipoProductoResource.setNombre(producto.getTipoProducto().getNombre());
		productoResource.setTipoProducto(tipoProductoResource);
		productoResource.setActivo(producto.getActivo());
				
		return productoResource;
	}
	

	
}
